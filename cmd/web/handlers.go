package main

import (
	"dauka_auth/pkg/model"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
)


func GenerateToken(user model.User) (string, error) {
	var err error
	secret := "golang_is_the_best"

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id" : user.ID,
		"user_email" : user.Email,
	})

	tokenString, err := token.SignedString([]byte(secret))

	if err != nil {
		log.Fatal(err)
	}

	return tokenString, nil
}


func (app *application) loginUser(w http.ResponseWriter, r *http.Request)  {
	if r.Method != http.MethodPost{
		RespondWithError(w, http.StatusMethodNotAllowed, model.Error{Message: "not allowed"})
	}else{
	var user model.UserDTO
	var jwt model.JWT
	var error1 model.Error
	errr := json.NewDecoder(r.Body).Decode(&user)
	if errr != nil{
		error1.Message = "invalid json from user"
		RespondWithError(w, http.StatusBadRequest, error1)
		return
	}

	if user.Email == "" {
		error1.Message = "Email is missing."
		RespondWithError(w, http.StatusBadRequest, error1)
		return
	}

	if user.HashedPassword == "" {
		error1.Message = "Password is missing."
		RespondWithError(w, http.StatusBadRequest, error1)
		return
	}


	user1, err := app.users.Auth(user)
	if err != nil {
		error1.Message = err.Error()
		RespondWithError(w, http.StatusInternalServerError, error1)
		return
	}else{

		token, err := GenerateToken(user1)
		if err != nil {
			log.Fatal(err)
		}
		w.WriteHeader(http.StatusOK)
		jwt.Token = token

		ResponseJSON(w, jwt)
	}
	}
}


func (app *application) registerUser(w http.ResponseWriter, r *http.Request)  {

	if r.Method != http.MethodPost{
		RespondWithError(w, http.StatusMethodNotAllowed, model.Error{Message: "not allowed"})
	}else {
		var user model.UserDTO
		var error1 model.Error
		errr := json.NewDecoder(r.Body).Decode(&user)
		if errr != nil {
			error1.Message = "invalid json from user"
			RespondWithError(w, http.StatusBadRequest, error1)
			return
		}

		if user.Email == "" {
			error1.Message = "Email is missing."
			RespondWithError(w, http.StatusBadRequest, error1)
			return
		}

		if user.HashedPassword == "" {
			error1.Message = "Password is missing."
			RespondWithError(w, http.StatusBadRequest, error1)
			return
		}

		err := app.users.Insert(user)
		if err != nil {
			error1.Message = err.Error()
			RespondWithError(w, http.StatusInternalServerError, error1)
			return
		} else {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(true)
		}
	}

}


func ResponseJSON(w http.ResponseWriter, data interface{}) {
	json.NewEncoder(w).Encode(data)
}


func RespondWithError(w http.ResponseWriter, status int, error model.Error) {
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(error)
}