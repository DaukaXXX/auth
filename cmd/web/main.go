package main

import (
	"context"
	"dauka_auth/pkg/model/mongo_rep"
	"flag"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"os"
	"time"
)

type application struct {
	errorLog 			*log.Logger
	infoLog 			*log.Logger
	users 			*mongo_rep.UserModel
}





func (app *application) routes() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/login", app.loginUser)
	mux.HandleFunc("/register", app.registerUser)

	return mux
}

func main() {
	addr := flag.String("addr", ":4000", "HTTP network address")
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	clientOptions := options.Client().ApplyURI("mongodb://mongofinal:27017")
	client1, _ := mongo.Connect(ctx, clientOptions)


	app := &application{
		errorLog: errorLog,
		infoLog: infoLog,
		users: &mongo_rep.UserModel{DB: client1},
	}



	srv := &http.Server{
		Addr: *addr,
		ErrorLog: errorLog,
		Handler: app.routes(),
		IdleTimeout: time.Minute,
		ReadTimeout: 5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}



	infoLog.Printf("Starting server on %s", *addr)
	err := srv.ListenAndServe()
	errorLog.Fatal(err)
}

