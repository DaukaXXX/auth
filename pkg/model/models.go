package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	ID 		primitive.ObjectID			`json:"id" bson:"_id"`
	Name 	string						`json:"name" bson:"name"`
	Email 	string						`json:"email" bson:"email"`
	HashedPassword string				`json:"hashed_password" bson:"HashedPassword"`
}

type UserDTO struct {

	Name 	string						`json:"name" bson:"name"`
	Email 	string						`json:"email" bson:"email"`
	HashedPassword string				`json:"hashed_password" bson:"HashedPassword"`

}

type JWT struct {
	Token string						`json:"token"`
}

type Error struct {
	Message string 						`json:"message"`
}