package mongo_rep

import (
	"context"
	"dauka_auth/pkg/model"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type UserModel struct {
	DB  *mongo.Client
}


func (m *UserModel) Insert(newUser model.UserDTO) error {
	hashedPassword, err1 := bcrypt.GenerateFromPassword([]byte(newUser.HashedPassword), 12)
	if err1 != nil{
		return err1
	}

	collection := m.DB.Database("auth").Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)

	err := collection.FindOne(ctx, bson.M{"email": newUser.Email})
	if err.Err() != mongo.ErrNoDocuments {
		return errors.New("you can not register with same email")
	}

	newUser.HashedPassword = string(hashedPassword)

	_, errr := collection.InsertOne(ctx,newUser)
	if errr != nil{
		return errr
	}
	return nil
}

func (m *UserModel) Auth(newUser model.UserDTO) (model.User, error) {
	var inUser model.User
	collection := m.DB.Database("auth").Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)

	err := collection.FindOne(ctx, bson.M{"email": newUser.Email})
	if err.Err() == mongo.ErrNoDocuments {
		return model.User{}, errors.New("user not registered")
	}

	decodeErr := err.Decode(&inUser)
	if decodeErr != nil{
		return model.User{}, decodeErr
	}

	newErr := bcrypt.CompareHashAndPassword([]byte(inUser.HashedPassword), []byte(newUser.HashedPassword))
	if newErr != nil {
		if errors.Is(newErr, bcrypt.ErrMismatchedHashAndPassword) {
			return model.User{}, errors.New("Invalid password or email")
		} else {
			return model.User{}, newErr
		}
	}


	return inUser, nil
}



