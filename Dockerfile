FROM golang
ADD . /usr/src/service-1
WORKDIR /usr/src/service-1/cmd/web
CMD ["go", "run", "."]